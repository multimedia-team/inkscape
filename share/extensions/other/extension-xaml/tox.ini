; SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
;
; SPDX-License-Identifier: GPL-2.0-or-later

# tox (https://tox.readthedocs.io/) is a tool for running tests
# in multiple virtualenvs. This configuration file will run the
# test suite on all supported python versions. Installation:
#     pip install -r requirements-dev.txt
# and then run "tox" from this directory.
[tox]
skipsdist = True
envlist = py{38,39,310,311,312}-normal, black, types

[testenv]
# for using poetry and tox
# see https://python-poetry.org/docs/faq/#is-tox-supported
# see https://github.com/python-poetry/poetry/issues/1745
whitelist_externals =
    poetry
# see https://pypi.org/project/tox-run-before/
run_before =
    poetry export --with=dev --without-hashes --output={toxinidir}/requirements.txt
deps =
    -r {toxinidir}/requirements.txt
commands =
    pytest {posargs}

setenv = COVERAGE_FILE=.coverage-{env:TOX_ENV_NAME}

[testenv:black]
deps = black
skip_install = True
commands = black . --check --verbose --diff --color

[testenv:types]
# use mypy on lowest version to make sure that all problems are caught
basepython = python3.8
deps = mypy
skip_install = True
commands = mypy inkxaml --ignore-missing-imports --check-untyped-defs

