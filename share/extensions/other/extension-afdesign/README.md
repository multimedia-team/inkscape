<> SPDX-FileCopyrightText: 2024 Software Freedom Conservancy <info@sfconservancy.org>
<> SPDX-License-Identifier: GPL-2.0-or-later

# Affinity Designer Extension for [Inkscape]

This is the source code to open Affinity Designer files in [Inkscape] as an [Inkscape plugin][plugin].

[Inkscape]: https://inkscape.org/
[plugin]: https://inkscape.org/gallery/=extension/
